package main

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func handleIndex(w http.ResponseWriter, r *http.Request) { //w: http.ResponseWriter, r: *http.Request
	json.NewEncoder(w).Encode("{\"message\": \"Hello\"}")
}

func main() {
	r := mux.NewRouter()
	// entrega un puntero del router, guardamos en var r
	r.HandleFunc("/", handleIndex).Methods(http.MethodGet)
	//params -> path de ruta y una funcion
	// con el .method le decimos que tipo de metodo es... podemos usar las constantes del paquete http

	//Ahora crearemos una variable de server....
	srv := http.Server{
		Addr:    ":8081",
		Handler: r,
	}

	log.Println("Listening...")
	srv.ListenAndServe()
	//Ahora podemos ejecutar con go run main.go...
}
